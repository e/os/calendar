package com.android.calendar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import ws.xsoh.etar.R;

/**
 * Created by Gitsaibot on 01.07.16.
 */
public class DynamicTheme {

    private static final String THEME_PREF = "pref_theme";
    private static final String COLOR_PREF = "pref_color";
    private static final String LIGHT = "light";
    private static final String DARK = "dark";
    private static final String BLACK = "black";
    private static final String TEAL = "teal";
    private static final String BLUE = "blue";
    private static final String ORANGE = "orange";
    private static final String GREEN = "green";
    private static final String RED = "red";
    private static final String PURPLE = "purple";
    private int currentTheme;

    public void onCreate(Activity activity) {
        currentTheme = getSelectedTheme(activity);
        activity.setTheme(currentTheme);
    }

    public void onResume(Activity activity) {
    }

    private static String getTheme(Context context) {
        /**
         * Migrate the user preferences to light mode.
         * It makes sure we don't unnecessarily access a wrong resource as we have disabled dark/black theme.
         */
        if (Utils.getSharedPreference(context, THEME_PREF, LIGHT).equalsIgnoreCase(BLACK)
                || Utils.getSharedPreference(context, THEME_PREF, LIGHT).equalsIgnoreCase(DARK)) {
            Utils.setSharedPreference(context, THEME_PREF, LIGHT);
        }
        return Utils.getSharedPreference(context, THEME_PREF, LIGHT);

    }

    private static int getSelectedTheme(Activity activity) {
        // String theme = getTheme(activity) + getPrimaryColor(activity);
        return R.style.CalendarAppThemeLightTeal;
    }

    public static String getPrimaryColor(Context context) {
        return Utils.getSharedPreference(context, COLOR_PREF, TEAL);
    }

    private static String getSuffix(String theme) {
        switch (theme) {
            case LIGHT:
                return "";
            case DARK:
            case BLACK:
                return "_" + theme;
            default:
                throw new IllegalArgumentException("Unknown theme: " + theme);
        }
    }

    public static int getColorId(String name) {
        switch (name) {
            case TEAL:
                return R.color.colorPrimary;
            case BLUE:
                return R.color.colorBluePrimary;
            case ORANGE:
                return R.color.colorOrangePrimary;
            case GREEN:
                return R.color.colorGreenPrimary;
            case RED:
                return R.color.colorRedPrimary;
            case PURPLE:
                return R.color.colorPurplePrimary;
            default:
                throw new UnsupportedOperationException("Unknown color name : " + name);
        }
    }

    public static String getColorName(int id) {
        switch (id) {
            case R.color.colorPrimary:
                return TEAL;
            case R.color.colorBluePrimary:
                return BLUE;
            case R.color.colorOrangePrimary:
                return ORANGE;
            case R.color.colorGreenPrimary:
                return GREEN;
            case R.color.colorRedPrimary:
                return RED;
            case R.color.colorPurplePrimary:
                return PURPLE;
            default:
                throw new UnsupportedOperationException("Unknown color id : " + id);
        }
    }

    public static int getColor(Context context, String id) {
        /*String suffix = getSuffix(getTheme(context));*/
        Resources res = context.getResources();
        /*Log.d("DynamicTheme", "Resource id: " + (id + suffix));*/
        return res.getColor(res.getIdentifier(id, "color", context.getPackageName()));
    }

    public static int getDrawableId(Context context, String id) {
        String suffix = getSuffix(getTheme(context));
        Resources res = context.getResources();
        return res.getIdentifier(id + suffix, "drawable", context.getPackageName());
    }
}